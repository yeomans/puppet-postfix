class postfix::config
    (
    Hash $aliases,
    String $mailname=$::fqdn,
    Hash $header_checks={},
    Optional[Hash] $main,
    Hash $virtual_aliases={},
    Hash $virtual_mailbox_domains={},
    Hash $virtual_mailbox_users={},
    )
    {
    # when $main is undef, we don't overwrite the default main.cf as supplied by postfix.   
    if $main != undef
        {
        file
            {
            "/etc/postfix/main.cf":
            ensure => file, 
            owner => 'root', 
            group => 'root', 
            mode => '0644', 
            content => template("postfix/main.cf.erb"),
            notify => Class[postfix::service]
            }
        }
    
    file
        {
        "/etc/aliases":
        ensure => file, 
        owner => 'root', 
        group => 'root', 
        mode => '0644', 
        content => template("postfix/aliases.erb"),
        } ~>
    exec
        {
        "/usr/bin/newaliases":
        refreshonly => true,
        }

    file
        {
        "/etc/mailname":
        ensure => file, 
        owner => 'root', 
        group => 'root', 
        mode => '0644', 
        content => template("postfix/mailname.erb"),
        }

    file
        {
        "/etc/postfix/header_checks":
        ensure => file, 
        owner => 'root', 
        group => 'root', 
        mode => '0644', 
        content => template("postfix/header_checks.erb"),
        }

    file
        {
        "/etc/postfix/virtual_aliases":
        ensure => file, 
        owner => 'root', 
        group => 'root', 
        mode => '0644', 
        content => template("postfix/virtual_aliases.erb"),
        notify => Exec["/usr/sbin/postmap /etc/postfix/virtual_aliases"],
        }        

    exec
        {
        "/usr/sbin/postmap /etc/postfix/virtual_aliases":
        refreshonly => true,
        }
        
    #note - need to make a Maildir for each mail address.         
    file
        {
        "/etc/postfix/virtual_mailbox_domains":
        ensure => file, 
        owner => 'root', 
        group => 'root', 
        mode => '0644', 
        content => template("postfix/virtual_mailbox_domains.erb"),
        notify => Exec["/usr/sbin/postmap /etc/postfix/virtual_mailbox_domains"],
        }        

    exec
        {
        "/usr/sbin/postmap /etc/postfix/virtual_mailbox_domains":
        refreshonly => true,
        }

    file
        {
        "/etc/postfix/virtual_mailbox_users":
        ensure => file, 
        owner => 'root', 
        group => 'root', 
        mode => '0644', 
        content => template("postfix/virtual_mailbox_users.erb"),
        notify => Exec["/usr/sbin/postmap /etc/postfix/virtual_mailbox_users"],
        }        

    exec
        {
        "/usr/sbin/postmap /etc/postfix/virtual_mailbox_users":
        refreshonly => true,
        }
    }