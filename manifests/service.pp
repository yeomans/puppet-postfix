class postfix::service($refreshonly=true)
    {
    if $refreshonly
        {
        exec
            {
            "systemctl restart postfix":
            path => $path,
            refreshonly => true,
            }
        }
    else
        {
        service{'postfix': ensure=>running}
        }
    }