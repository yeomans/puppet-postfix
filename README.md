# Puppet-Postfix

Puppet-Postfix is a starter implementation of a Puppet module suitable for my use.  One bit 
that may be of interest is the postfix::service class, which implements a refreshonly service 
for those who, like me, don't want Puppet to manage service beyond a HUP when the 
configuration is changed.
